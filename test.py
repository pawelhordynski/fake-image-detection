import torch
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
import os
import sys
import cv2

print(torch.cuda.is_available())
cwd = os.chdir(os.getcwd()) 

image_path = 'data/fake/84fd5a243a63e25013ef3d6fe8eeaf12.png'

image = mpimg.imread(image_path)

sobel_y = np.array([
    [-1,-2,-1],
    [ 0, 0, 0],
    [ 1, 2, 1]
])

gray = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)

filtered_image = cv2.filter2D(gray, -1, sobel_y)

plt.imshow(filtered_image, cmap='gray')

plt.show()




