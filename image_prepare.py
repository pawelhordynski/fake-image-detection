import pandas as pd
import pickle
import matplotlib.image
import matplotlib.pyplot as plt
import torch
import os
from imageio import imread
from sklearn.model_selection import train_test_split
import cv2
import numpy as np


fake_path = 'data/fake/'
fake_mask_path = 'data/fake/masks/'
pristine_path = 'data/pristine/'

fake_path = 'data/fake/'
fake_mask_path = 'data/fake/masks/'
pristine_path = 'data/pristine/'

pristine_images_list = os.listdir(pristine_path)

masks = os.listdir(fake_mask_path)
fakes = os.listdir(fake_path)
pristines = os.listdir(pristine_path)

with open('pickle/one_channel_pristines.pickle', 'rb') as f:
    one_channel_pristines = pickle.load(f)
    
with open('pickle/three_channel_pristines.pickle', 'rb') as f:
    three_channel_pristines = pickle.load(f)

with open('pickle/four_channel_pristines.pickle', 'rb') as f:
    four_channel_pristines=pickle.load(f)
    
with open('pickle/three_channel_fakes.pickle', 'rb') as f:
    three_channel_fakes=pickle.load(f)
    
with open('pickle/four_channel_fakes.pickle', 'rb') as f:
    four_channel_fakes=pickle.load(f)

with open('pickle/one_channel_masks.pickle', 'rb') as f:
    one_channel_masks=pickle.load(f)
    
with open('pickle/three_channel_masks.pickle', 'rb') as f:
    three_channel_masks=pickle.load(f)
    
with open('pickle/four_channel_masks.pickle', 'rb') as f:
    four_channel_masks=pickle.load(f)


## testy wielokanałowych obrazów
#t = imread(fake_path+four_channel_fakes[49])
#
#t = four_channel_image = imread(fake_mask_path+four_channel_masks[54])
#
#plt.imshow(t[:,:,2])
#
#plt.show()


pristines_final=[]
for pristine in pristines:
    try:
        img=imread(pristine_path+pristine)
        if len(img.shape)<3:
            continue
        if img.shape[2]==4:
            continue
        pristines_final.append(pristine)
    except:
        pass

print(len(pristines_final))

fake_images=[]
fakes_final=[]
for fake in fakes:
    try:
        img=imread(fake_path+fake)
        try:
            fake_images.append(img[:,:,:3])
            fakes_final.append(fake)
        except IndexError:
            print(f'image {fake} has only 1 channel')
    except:
        pass

print(len(fake_images))

image_names=[]
for i in range(0, len(pristines_final)):
    image_names.append(pristines_final[i])
for i in range(0, len(fake_images)):
    image_names.append(fakes_final[i])

print(len(image_names))

labels=[0]*1025+[1]*450

x_train, x_test, y_train, y_test = train_test_split(image_names, labels, test_size=0.2, stratify=labels)

x_train_images=[]

for x in x_train:
    try:
        img=imread(pristine_path+x)
    except FileNotFoundError:
        img=imread(fake_path+x)
    
    x_train_images.append(img)


x_train_mask_names=[]
for ind, x in enumerate(x_train):
    if y_train[ind]==1:
        x_train_mask_names.append(x.split('.')[0]+'.mask.png')

x_train_fakes_names=[]
x_train_fake_images=[]
for ind, x in enumerate(x_train):
    if y_train[ind]==1:
        x_train_fakes_names.append(x)
        x_train_fake_images.append(x_train_images[ind])


x_train_pristines_names=[]
x_train_pristine_images=[]
for ind, x in enumerate(x_train):
    if y_train[ind]==0:
        x_train_pristines_names.append(x)
        x_train_pristine_images.append(x_train_images[ind])

x_train_masks=[]
for m in x_train_mask_names:
    
    img=imread(fake_mask_path+m)
    
    if len(img.shape)>2:
        img=img[:,:,0]
        
    x_train_masks.append(img)


binaries=[]

for grayscale in x_train_masks:
    blur = cv2.GaussianBlur(grayscale,(5,5),0)
    ret,th = cv2.threshold(blur,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    binaries.append(th)

def count_255(mask):
    i=0
    for row in range(mask.shape[0]):
        for col in range(mask.shape[1]):
            if mask[row,col]==255:
                i+=1
    return i

def sample_fake(img, mask):
    kernel_size=64
    stride=32
    
    samples=[]
    
    for y_start in range(0, img.shape[0]-kernel_size+1, stride):
        for x_start in range(0, img.shape[1]-kernel_size+1, stride):
            
            c_255=count_255(mask[y_start:y_start+kernel_size, x_start:x_start+kernel_size])
            
            if (c_255>1600) and (kernel_size*kernel_size-c_255>1600):
                samples.append(img[y_start:y_start+kernel_size, x_start:x_start+kernel_size, :3])    
        
    return samples

def sample_random(img, num_samples, stride=8):
    kernel_size=64
    
    x_start=0
    y_start=0
    samples=[]
    
    for y_start in range(0, img.shape[0] - kernel_size + 1, stride):
        for x_start in range(0, img.shape[1] - kernel_size + 1, stride):

            #c_255 = count_255(mask[y_start:y_start + kernel_size, x_start:x_start + kernel_size])

            #if (c_255 > threshold) and (kernel_size * kernel_size - c_255 > threshold):
            samples.append(img[y_start:y_start + kernel_size, x_start:x_start + kernel_size, :3])

    
    indices=np.random.randint(0, len(samples), min(len(samples), num_samples))
    
    sampled=[]
    for i in indices:
        sampled.append(samples[i])
    
    return sampled

#plt.imshow(binaries[28], cmap='gray')
#plt.show()

mask_pixels = [np.count_nonzero(~binaries[i]) for i in range(len(binaries))]
x_test_pristine_names=x_test[y_test==0]
x_test_pristine=x_test[y_test==0]
x_test_fake=x_test[y_test==1]

x_test_pristine = []
for img_name in x_test_pristine_names:
    if os.path.isfile(pristine_path + img_name):
        pristine_img = imread(pristine_path + img_name)
        x_test_pristine.append(pristine_img[:, :, :3])

len(x_test_pristine_names)
len(x_test_fake)/len(x_test_pristine_names)
print(249*205)

samples_pristine_binary=np.ndarray(shape=(51045, 64, 64, 3), dtype=np.dtype('uint8'))

#sample=sample_fake(x_train_fake_images[5], x_train_masks[5])

print(x_train_fake_images[5].shape)

print(x_train_masks[5].shape)


print(len(x_train_pristine_images)* 6)

samples_pristine_grayscale=np.ndarray(shape=(4920, 64, 64, 3), dtype=np.dtype('uint8'))

i=0

for pristine_img in x_train_pristine_images:
    
    samples=sample_random(pristine_img, 6, stride=32)
    for sample in samples:
        samples_pristine_grayscale[i, :, :, :]=sample
        i+=1

print(i)


# samples_fake=[]
# i=0
# for fake, mask in zip(x_train_fake_images, x_train_masks):
#     image_samples=sample_fake(fake, mask)
#     for sample in image_samples:
#         samples_fake.append(sample)
#         i+=1


np.save('data/k64 grayscale 40percent stride32/samples_pristine.npy', samples_pristine_grayscale)
np.save('data/k64 binary 25percent stride8/samples_pristine.npy', samples_pristine_binary)

with open('data/x_train_fake_images.pickle', 'wb') as f:
    pickle.dump(x_train_fake_images, f)

with open('data/x_train_images.pickle', 'wb') as f:
    pickle.dump(x_train_images, f)
        
with open('data/x_train_pristine_images.pickle', 'wb') as f:
    pickle.dump(x_train_pristine_images, f)

with open('data/x_train_masks.pickle', 'wb') as f:
    pickle.dump(x_train_masks[:50], f)

with open('data/x_train_masks_1.pickle', 'wb') as f:
    pickle.dump(x_train_masks[50:100], f)

with open('data/x_train_masks_2.pickle', 'wb') as f:
    pickle.dump(x_train_masks[100:150], f)

with open('data/x_train_masks_3.pickle', 'wb') as f:
    pickle.dump(x_train_masks[150:200], f)

with open('data/x_train_masks_4.pickle', 'wb') as f:
    pickle.dump(x_train_masks[200:250], f)

with open('data/x_train_masks_5.pickle', 'wb') as f:
    pickle.dump(x_train_masks[250:300], f)

with open('data/x_train_masks_6.pickle', 'wb') as f:
    pickle.dump(x_train_masks[300:350], f)

with open('data/x_train_masks_7.pickle', 'wb') as f:
    pickle.dump(x_train_masks[350:400], f)


#train_labels_binary=[0]*len(samples_pristine_binary)+[1]*len(samples_fake_binary)
#train_labels_grayscale=[0]*len(samples_pristine_grayscale)+[1]*len(samples_fake_grayscale)

# with open('k64 binary 25percent stride8/train_labels.pickle', 'wb') as f:
#     pickle.dump(train_labels_binary, f)
    
# with open('k64 grayscale 40percent stride32/train_labels.pickle', 'wb') as f:
#     pickle.dump(train_labels_grayscale, f)

with open('data/x_train_fakes_names.pickle', 'wb') as f:
    pickle.dump(x_train_fakes_names, f)

# with open('x_train_mask_names.pickle', 'wb') as f:
#     pickle.dump(x_train_mask_names, f)

# with open('x_train_pristines_names.pickle', 'wb') as f:
#     pickle.dump(x_train_pristines_names, f)
