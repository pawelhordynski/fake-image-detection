import torch
import os
import numpy as np
from imageio import imread
import pandas as pd
import pickle

fake_path = 'data/fake/'
fake_mask_path = 'data/fake/masks/'
pristine_path = 'data/pristine/'

pristine_images_list = os.listdir(pristine_path)

masks = os.listdir(fake_mask_path)
fakes = os.listdir(fake_path)
pristines = os.listdir(pristine_path)

fake_shapes=[]


# ---- PREPARE FAKES ------ #

# wszystkie fejki

fake_shapes=[]
for fake in fakes:
    try:
        path = fake_path+fake
        img=imread(path)
        fake_shapes.append(img.shape)
    except:
        print('problem z odcztytem ' + path)

#wsztstkie maski

mask_shapes=[]
for mask in masks:
    try:
        path = fake_mask_path+mask
        img=imread(path)
        mask_shapes.append(img.shape)
    except:
        print('problem z odcztytem ' + path)

#wszystkie nieskazitelne

pristines_shapes=[]
for pristine in pristines:
    try:
        path = pristine_path+pristine
        img=imread(path)
        pristines_shapes.append(img.shape)
    except:
        print('problem z odcztytem ' + path)


# czetero-kanałowe fejki

four_channel_fakes=[]

for fake in fakes:
    try:
        path = fake_path+fake
        img=imread(path)
        if imread(path).shape[2]==4:
            four_channel_fakes.append(fake)
    except:
        print('problem z odcztytem ' + path)

# trzy-kanałowe fejki

three_channel_fakes = []

for fake in fakes:
    try:
        path = fake_path+fake
        img=imread(path)
        if img.shape[2]==3:
            three_channel_fakes.append(imread(path).shape)
    except:
        print('problem z odcztytem' + path)
 

# ---- PREPARE MASKS ------ #

one_channel_masks=[]

for mask in masks:
    try:
        path = fake_mask_path+mask
        img=imread(path)
        if len(img.shape)==2:
            one_channel_masks.append(mask)
    except:
        print('problem z odcztytem' + path)

three_channel_masks=[]

for mask in masks:
    try:
        path = fake_mask_path+mask
        img=imread(path)
        if img.shape[2]==3:
            three_channel_masks.append(mask)
    except:
        print('problem z odcztytem' + path)

four_channel_masks=[]

for mask in masks:
    try:
        path = fake_mask_path+mask
        img=imread(path)
        if img.shape[2]==4:
            four_channel_masks.append(mask)
    except:
        print('problem z odcztytem' + path)

# ----- PREPARE PRISTINES ----- #


four_channel_pristines=[]

for pristine in pristines:
    try:
        path = pristine_path+pristine
        img=imread(path)
        if img.shape[2]==4:
            four_channel_pristines.append(pristine)
    except:
        print('problem z odcztytem' + path)


three_channel_pristines=[]


for pristine in pristines:
    try:
        path = pristine_path+pristine
        img=imread(path)
        if img.shape[2]==3:
            three_channel_pristines.append(pristine)
    except:
        print('problem z odcztytem' + path)

one_channel_pristines=[]

for pristine in pristines:
    try:
        img=imread(pristine_path+pristine)
        if len(img.shape)<3:
            one_channel_pristines.append(pristine)
    except:
        print('problem z odcztytem' + pristine_path+pristine)


# zapisy

with open('pickle/one_channel_pristines.pickle', 'wb') as f:
    pickle.dump(one_channel_pristines, f)
    
with open('pickle/three_channel_pristines.pickle', 'wb') as f:
    pickle.dump(three_channel_pristines, f)

with open('pickle/four_channel_pristines.pickle', 'wb') as f:
    pickle.dump(four_channel_pristines, f)
    
with open('pickle/three_channel_fakes.pickle', 'wb') as f:
    pickle.dump(three_channel_fakes, f)
    
with open('pickle/four_channel_fakes.pickle', 'wb') as f:
    pickle.dump(four_channel_fakes, f)

with open('pickle/one_channel_masks.pickle', 'wb') as f:
    pickle.dump(one_channel_masks, f)
    
with open('pickle/three_channel_masks.pickle', 'wb') as f:
    pickle.dump(three_channel_masks, f)
    
with open('pickle/four_channel_masks.pickle', 'wb') as f:
    pickle.dump(four_channel_masks, f)


# test odczytów

with open('pickle/one_channel_pristines.pickle', 'rb') as f:
    one_channel_pristines = pickle.load(f)
    
with open('pickle/three_channel_pristines.pickle', 'rb') as f:
    three_channel_pristines = pickle.load(f)

with open('pickle/four_channel_pristines.pickle', 'rb') as f:
    four_channel_pristines=pickle.load(f)
    
with open('pickle/three_channel_fakes.pickle', 'rb') as f:
    three_channel_fakes=pickle.load(f)
    
with open('pickle/four_channel_fakes.pickle', 'rb') as f:
    four_channel_fakes=pickle.load(f)

with open('pickle/one_channel_masks.pickle', 'rb') as f:
    one_channel_masks=pickle.load(f)
    
with open('pickle/three_channel_masks.pickle', 'rb') as f:
    three_channel_masks=pickle.load(f)
    
with open('pickle/four_channel_masks.pickle', 'rb') as f:
    four_channel_masks=pickle.load(f)